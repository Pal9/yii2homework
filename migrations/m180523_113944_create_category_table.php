<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m180523_113944_create_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer()->defaultValue(0),
            'title' => $this->string()->notNull(),
            'description' => $this->text(),
            'is_active' => $this->boolean()->defaultValue(0),
        ]);

        $this->addForeignKey('cat_to_cat', 'category', 'parent_id', 'category', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('category');
    }
}
