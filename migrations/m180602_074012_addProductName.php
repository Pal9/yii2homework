<?php

use yii\db\Migration;

/**
 * Class m180602_074012_addProductName
 */
class m180602_074012_addProductName extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$this->addColumn('product', 'name', 'string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180602_074012_addProductName cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180602_074012_addProductName cannot be reverted.\n";

        return false;
    }
    */
}
