<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m180523_113959_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer(),
            'title' => $this->string()->notNull(),
            'description' => $this->text(),
            'price' => $this->integer(),
            'quantity' => $this->integer(),
            'is_active' => $this->boolean()->defaultValue(0),
        ]);

        $this->addForeignKey('prod_to_cat', 'product', 'parent_id', 'category', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product');
    }
}
