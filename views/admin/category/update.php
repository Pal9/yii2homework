<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\Category */

if (empty($model->id)) {
    $this->title = 'Создание категории';
    $this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
} else {
    $this->title = 'Обновление категории: ' . $model->title;
    $this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = 'Обновление';
}

?>
<div class="category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'categories' => $categories,
    ]) ?>

</div>
